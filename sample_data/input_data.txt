# File parsing supports comments for lines starting with #
# Inline comments are not supported
# There cannot be comments between the key and value data
# All keys and values have to be in a single line. Multi-line statements are not supported.
# Leave an empty line to denote end of data set.
# No restrictions for keys to be consistent between data sets.
name
Pankaj Singh
status
online

name
Foo Bar
gender
unknown

name
Alpha
# XML structured to support keys with spaces.
phone number
123 456 7890
# Leave an empty line at the end
