#!/usr/bin/env python

"""In Python write a command line tool which shows how you would take some sets
of personal data (name, address, phone number) and serialise/deserialise them
in at least 2 formats (one should be XML), and display it in at least
2 different ways (no need to use a GUI Framework, command line is fine).

There is no need to support manual data entry - you could manually write a file
in one of your chosen formats to give you your test data.

Write it in such a way that it would be easy for a developer:
>>      to add support for additional storage formats
>>      to query a list of currently supported formats
>>      to supply an alternative XML reader/writer
"""

import os
import argparse
from reformat_users.users import get_users_data
from reformat_users.users_serializer import UsersSerializer


class CustomArgParseFormatter(argparse.RawDescriptionHelpFormatter,
                              argparse.ArgumentDefaultsHelpFormatter):
    """Custom ArgParse formatter to support multi-line description and show
    default arguments.
    """
    def __init__(self, *args, **kwargs):
        super(CustomArgParseFormatter, self).__init__(*args, **kwargs)


tool_description = """
Users data serialization/deserialization application.

Sample XML format :-
<users>
    <user>
        <data>
            <key>name</key>
            <value>Pankaj Singh</value>
        </data>
    </user>
</users>

Sample JSON format :-
'[{"name": "Pankaj Singh", "status": "Online"}]'

Sample usage :-
1.> reformat_users_app.py -h
2.> reformat_users_app.py -show_converters
3.> reformat_users_app.py -serialize
4.> reformat_users_app.py -serialize -converter json
5.> reformat_users_app.py -serialize -save <file path of file to save>
6.> reformat_users_app.py -serialize -file <path to load data from custom file>
7.> reformat_users_app.py -converter xml -ds "<users><user><data><key>status</key><value>online</value></data><data><key>name</key><value>Pankaj Singh</value></data></user></users>"
8.> reformat_users_app.py -converter xml -df <path to existing xml file>

"""

data_file = os.path.dirname(os.path.abspath(__file__)) + "/sample_data/input_data.txt"

parser = argparse.ArgumentParser(description=tool_description,
                                 formatter_class=CustomArgParseFormatter)
parser.add_argument("-show_converters", "-sc", action="store_true", default=False,
                    help="Show available converters.")
parser.add_argument("-converter", "-c", action="store", default="xml",
                    help="Name of the converter to use for serializing.")
parser.add_argument("-file", "-f", action="store", default=data_file,
                    help="File with users data to be serialized.")
parser.add_argument("-serialize", "-sr", action="store_true", default=False,
                    help="Serialize users data.")
parser.add_argument("-save", "-s", action="store", dest="output_file_path",
                    help="File to save serialized data.")
parser.add_argument("-deserialize_string", "-ds", action="store", dest="deserialize_string",
                    default="", help="Deserialize the input string.")
parser.add_argument("-deserialize_file", "-df", action="store", dest="deserialize_file_path",
                    help="Load serialized data from file.")

arguments = parser.parse_args()

if arguments.show_converters:
    print("Available converters are %s" % ", ".join(UsersSerializer().valid_converters()))
    # Executing the rest does not make sense if the user is not aware of converters
    exit(0)

serializer = UsersSerializer(arguments.converter)

if arguments.deserialize_string:
    print("Deserialized data from string is \n%s" % serializer.deserialize(arguments.deserialize_string))
    exit(0)

if arguments.deserialize_file_path:
    print("Deserialized data from file path is \n%s" % serializer.load(arguments.deserialize_file_path))
    exit(0)

if arguments.serialize:
    users = get_users_data(arguments.file)
    if arguments.output_file_path:
        serializer.dump(users, arguments.output_file_path)
        print("Saved serialized data to %s" % os.path.abspath(arguments.output_file_path))
    else:
        print("Serialized data is \n%s" % serializer.serialize(users))
