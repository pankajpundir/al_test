from lxml.etree import ElementTree, Element, XML, tostring, parse
from .xml_converter import XMLConverter


class LXMLConverter(XMLConverter):
    """Class for serializing using an lxml object.
    Using Dependency Injection for reusing XMLConverter class.
    """

    name = "lxml"

    def __init__(self):
        super(LXMLConverter, self).__init__()

    def serialize(self, obj, elem_obj=Element, serial_func=tostring):
        """Serialize the object into valid XML as string representation.

        :param obj: Class object to be serialized.
        :param elem_obj: DI for Element object.
        :param serial_func: DI for tostring function.
        :returns str: Valid XML as string.
        """
        return super(LXMLConverter, self).serialize(obj, elem_obj, serial_func)

    def dump(self, obj, file_path, elem_obj=Element, tree_obj=ElementTree):
        """Function to serialized data as per converter to a file.

        :param obj Users object to be serialized.
        :param file_path File path to save serialized data.
        :param elem_obj DI for Element object.
        :param tree_obj ElementTree object used to save the XML on disk.
        :returns Status of writing the file object.
        """
        return super(LXMLConverter, self).dump(obj, file_path, elem_obj, tree_obj)

    def deserialize(self, serial_data, xml_obj=XML):
        """Return a list of dict from a serialized XML.

        :param serial_data: Serialized XML string.
        :param xml_obj: DI for XML object.
        :returns list: List of Dict.
        """
        return super(LXMLConverter, self).deserialize(serial_data, XML)

    def load(self, file_path, parse_func=parse):
        """Function to load serialized data from a file.

        :param file_path, File to load data from.
        :param parse_func Function used to parse the XML from file.
        :returns List of Dict.
        """
        return super(LXMLConverter, self).load(file_path, parse_func)
