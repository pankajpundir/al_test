from xml.etree.ElementTree import ElementTree, Element, XML, tostring, parse
from .base_converter import BaseConverter

def create_xml(input_data, xml_elem=Element):
    """Convert input data into valid xml.etree.ElementTree object

    :param input_data List of dict to be converted into xml format.
    :param xml_elem XML Element object, dependency injection.

    :returns XML element of type xml.etree.ElementTree.ElementTree
    """
    root = xml_elem("users")

    for user_data in input_data:
        user = xml_elem("user")
        for key, value in user_data.iteritems():
            data = xml_elem("data")

            key_elem = xml_elem("key")
            key_elem.text = key

            val_elem = xml_elem("value")
            val_elem.text = value

            data.extend([key_elem, val_elem])
            user.append(data)
        root.append(user)

    return root

def get_xml(xml_string, xml_obj=XML):
    """Function to create XML object from string

    :param xml_string Valid XML string.
    :param xml_obj Object of type xml.etree.ElementTree.XML, DI
    :returns Valid XML object of type xml.etree.ElementTree.XML
    """
    return xml_obj(xml_string)

def serialize_xml(xml, serial_func=tostring):
    """Function to serialize the XML object
    :param xml root object of type xml.etree.ElementTree.Element
    :param serial_func xml.etree.ElementTree.tostring function to serialize XML.
    :returns Serialized XML.
    """
    return serial_func(xml)

def write_xml(element, file_path, tree=ElementTree):
    """Function to create XML object from string

    :param element Root element of the XML.
    :param file_path File to save the XML.
    :param tree ElementTree object to write the XML.
    :returns Status of saving the XML to disk.
    """
    return tree(element).write(file_path)

def parse_xml(file_path, parse_func=parse):
    """Function to load a XML file from disk

    :param file_path File to load XML from.
    :param parse_func Function to use to load the XML file.
    :returns XML object
    """
    return parse_func(file_path)


class XMLConverter(BaseConverter):
    """Class to serialize data to xml.
    Using Dependency Injection for extensibility/reusability with other xml
    packages like lxml.
    """

    name = "xml"

    def __init__(self):
        super(XMLConverter, self).__init__()

    def serialize(self, obj, elem_obj=Element, serial_func=tostring):
        """Serialize the object into valid XML as string representation.

        :param obj Class object to be serialized.
        :param elem_obj DI for Element object.
        :param serial_func DI for tostring function.
        :returns Valid XML as string.
        """
        return serialize_xml(create_xml(obj, elem_obj), serial_func)

    def dump(self, obj, file_path, elem_obj=Element, tree_obj=ElementTree):
        """Function to serialized data as per converter to a file.

        :param obj Users object to be serialized.
        :param file_path File path to save serialized data.
        :param elem_obj DI for Element object.
        :param tree_obj ElementTree object used to save the XML on disk.
        :returns Status of writing the file object.
        """
        return write_xml(create_xml(obj, elem_obj), file_path, tree_obj)

    def deserialize(self, serial_data, xml_obj=XML):
        """Return a list of dict from a serialized XML.

        :param serial_data Serialized XML string.
        :param xml_obj DI for XML object.
        :returns List of Dict.
        """
        root = get_xml(serial_data, xml_obj)
        return [{data[0].text: data[1].text for data in user} for user in root]

    def load(self, file_path, parse_func=parse):
        """Function to load serialized data from a file.

        :param file_path File to load data from.
        :param parse_func Function used to parse the XML from file.
        :returns List of Dict.
        """
        root = parse_xml(file_path, parse_func).getroot()
        return [{data[0].text: data[1].text for data in user} for user in root]
