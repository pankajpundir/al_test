class BaseConverter(object):

    name = "base"

    def __init__(self):
        super(BaseConverter, self).__init__()

    def serialize(self, obj):
        """Serialize the object.

        :param obj: Class object to be serialized.
        :returns str: Should return a string.
        """
        raise NotImplementedError

    def dump(self, users, file_path):
        """Function to save list of users as per converter to a file.

        :param users: Users object to be serialized.
        :param file_path: File path to save serialized data.

        :returns bool: Status of writing the file object.
        """
        raise NotImplementedError

    def deserialize(self, serial_data):
        """Return a list of dict from a string.

        :param serial_data: Serialized json.
        :returns list: Should return a List of Dict.
        """
        raise NotImplementedError

    def load(self, file_path):
        """Function to load serialized data from a file.

        :param file_path, File to load data from.
        :returns Users, Deserialized Users object.
        """
        raise NotImplementedError