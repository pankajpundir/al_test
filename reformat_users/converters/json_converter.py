import json
from .base_converter import BaseConverter


class JSONConverter(BaseConverter):
    """Converter to serialize and deserialize data to/from JSON format"""

    name = "json"

    def __init__(self):
        super(JSONConverter, self).__init__()

    def serialize(self, obj):
        """Serialize the object.

        :param obj Class object to be serialized
        :returns Serialized JSON.
        """
        return json.dumps(obj)

    def dump(self, obj, file_path):
        """Function to serialized data as per converter to a file.

        :param obj Users object to be serialized.
        :param file_path File path to save serialized data.

        :returns Status of writing the file object.
        """
        with open(file_path, "w") as fh:
            return json.dump(obj, fh)

    def deserialize(self, serial_data):
        """Return a list of dict from a string

        :param serial_data Serialized json.
        :returns List of Dict
        """
        return json.loads(serial_data)

    def load(self, file_path):
        """Function to load serialized data from a file.

        :param file_path, File to load data from.
        :returns Deserialized Users object.
        """
        with open(file_path, "r") as fh:
            return json.load(fh)
