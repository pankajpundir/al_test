import os
from exceptions import Exception
from .users import Users
from .converters import XMLConverter, JSONConverter, BaseConverter, LXMLConverter


class InvalidConverter(Exception):
    pass


class UsersSerializer(object):
    """Abstract implementation of converters."""

    _converters = (XMLConverter, JSONConverter, LXMLConverter)

    @classmethod
    def _validate_converters(cls):
        """Keep only valid converters"""
        valid_converters = []
        for converter in cls._converters:
            if (isinstance(converter, BaseConverter) or
                (callable(getattr(converter, "serialize")) and
                 callable(getattr(converter, "deserialize")))):
                valid_converters.append(converter)

        cls._converters = tuple(valid_converters)

    def __init__(self, converter_type="xml"):
        super(UsersSerializer, self).__init__()
        self._validate_converters()

        # Set the requested converter.
        for converter in self._converters:
            if converter.name == converter_type.lower():
                self._converter = converter()
                break
        else:
            raise InvalidConverter("Unknown converter type %s" % converter_type)

    def valid_converters(self):
        """Function to get list of valid converter names

        :returns List of valid converter names.
        """
        return [converter.name for converter in self._converters]

    def serialize(self, users):
        """Function to serialize users object

        :param users Users object to be serialized
        :return Serialized Users object.
        """
        if not isinstance(users, Users):
            raise TypeError("Invalid object type. Expected type 'Users'.")

        return self._converter.serialize(users)

    def dump(self, users, file_path):
        """Function to save list of users as per converter to a file.

        :param users Users object to be serialized.
        :param file_path File path to save serialized data.
        :returns Status of writing the file object.
        """
        if not isinstance(users, Users):
            raise TypeError("Invalid object type. Expected type 'Users'.")

        return self._converter.dump(users, file_path)

    def deserialize(self, serial_data):
        """Function to deserialize input string as per converter.

        :param serial_data Input string that should deserialized
        :returns Deserialized Users object.
        """
        if not isinstance(serial_data, basestring):
            raise TypeError("Input data is expected to be of type string")

        return Users(self._converter.deserialize(serial_data))

    def load(self, file_path):
        """Function to load serialized data from a file.

        :param file_path File to load data from.
        :returns Deserialized Users object.
        """
        if not os.path.isfile(file_path):
            raise ValueError("%s is not a file." % file_path)

        return Users(self._converter.load(file_path))
