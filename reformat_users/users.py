import os


class Users(list):
    def __init__(self, iterable=None):
        super(Users, self).__init__(iterable)

    def __str__(self):
        return "\n".join(map(str, self))


def get_users_data(file_path):
    """Read input data and return it as a dict.

    :param file_path: str, Path of the file to parse

    :returns dict
    """
    if not os.path.isfile(file_path):
        if file_path:
            raise ValueError("Invalid file path %s" % file_path)

    data_collection = []
    with open(file_path, "r") as fh:
        line = "dummy"
        data = {}
        while line:
            line = fh.readline().strip(" ")

            # Supports comments
            if line.startswith("#"):
                continue

            # Empty line indicates start of new set of data
            if line in ["\n", ""]:
                if data:
                    data_collection.append(data)
                data = {}
                continue

            key = line.strip()
            value = fh.readline().strip()
            data[key] = value

    return Users(data_collection)
