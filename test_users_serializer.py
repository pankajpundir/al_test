#!/usr/bin/env python

import os
import json
import pytest
from reformat_users.users import get_users_data
from reformat_users.users_serializer import InvalidConverter, UsersSerializer
from reformat_users.converters import XMLConverter, JSONConverter, LXMLConverter

input_file = os.path.dirname(os.path.abspath(__file__)) + "/sample_data/input_data.txt"
sample_xml_file = os.path.abspath(os.path.dirname(__file__)) + "/sample_data/users.xml"
sample_json_file = os.path.abspath(os.path.dirname(__file__)) + "/sample_data/users.json"

users = get_users_data(input_file)

xml_serialized = "<users><user><data><key>status</key><value>online</value></data>" \
                 "<data><key>name</key><value>Pankaj Singh</value></data></user>" \
                 "<user><data><key>gender</key><value>unknown</value></data>" \
                 "<data><key>name</key><value>Foo Bar</value></data></user>" \
                 "<user><data><key>phone number</key><value>123 456 7890</value></data>" \
                 "<data><key>name</key><value>Alpha</value></data></user></users>"

json_serialized = '[{"status": "online", "name": "Pankaj Singh"},' \
                  ' {"gender": "unknown", "name": "Foo Bar"},' \
                  ' {"phone number": "123 456 7890", "name": "Alpha"}]'

# ----------------------------- XML -------------------------------------------
def test_xml_serialize():
    serializer = UsersSerializer('xml')
    assert serializer.serialize(users) == xml_serialized

def test_xml_deserialize():
    serializer = UsersSerializer('xml')
    assert serializer.deserialize(xml_serialized) == users

# ----------------------------- LXML ------------------------------------------
def test_lxml_serialize():
    serializer = UsersSerializer('lxml')
    assert serializer.serialize(users) == xml_serialized

def test_lxml_deserialize():
    serializer = UsersSerializer('lxml')
    assert serializer.deserialize(xml_serialized) == users

# ---------------------------- JSON -------------------------------------------
def test_json_serialize():
    serializer = UsersSerializer('json')
    assert serializer.serialize(users) == json_serialized

def test_json_deserialize():
    serializer = UsersSerializer('json')
    assert serializer.deserialize(json_serialized) == users

# -------------------------- Converters ---------------------------------------
def test_valid_converters():

    converters = [XMLConverter, JSONConverter, LXMLConverter]
    valid_converters = UsersSerializer().valid_converters()
    for converter in converters:
        assert converter.name in valid_converters

def test_invalid_converter():
    with pytest.raises(InvalidConverter) as excinfo:
        UsersSerializer("invalid")

    assert "Unknown converter type invalid" in str(excinfo.value)

# -------------------------- Input Data ---------------------------------------
def test_invalid_file_path():
    with pytest.raises(ValueError) as excinfo:
        get_users_data("/invalid/file/path")

    assert "Invalid file path" in str(excinfo.value)

# -------------------------- Save File ----------------------------------------
def test_save_xml():
    serializer = UsersSerializer('xml')
    saved_file = os.path.abspath(os.path.dirname(__file__)) + "/sample_data/new_users.xml"
    serializer.dump(users, saved_file)
    with open(saved_file, "r") as fha, open(sample_xml_file, "r") as fhb:
        assert fha.readlines() == fhb.readlines()
    os.remove(saved_file)

def test_save_lxml():
    serializer = UsersSerializer('lxml')
    saved_file = os.path.abspath(os.path.dirname(__file__)) + "/sample_data/new_lxml_users.xml"
    serializer.dump(users, saved_file)
    with open(saved_file, "r") as fha, open(sample_xml_file, "r") as fhb:
        assert fha.readlines() == fhb.readlines()
    os.remove(saved_file)

def test_save_json():
    serializer = UsersSerializer('json')
    saved_file = os.path.abspath(os.path.dirname(__file__)) + "/sample_data/new_users.json"
    serializer.dump(users, saved_file)
    with open(saved_file, "r") as fha, open(sample_json_file, "r") as fhb:
        assert json.load(fha) == json.load(fhb)
    os.remove(saved_file)

# -------------------- Deserialize from file ----------------------------------
def test_load_xml():
    serializer = UsersSerializer('xml')
    assert users == serializer.load(sample_xml_file)

def test_load_lxml():
    serializer = UsersSerializer('lxml')
    assert users == serializer.load(sample_xml_file)

def test_load_json():
    serializer = UsersSerializer('json')
    assert users == serializer.load(sample_json_file)

# --------------------------- Cross Format Conversion -------------------------
def test_cross_format_conversion():
    xml_serializer = UsersSerializer('xml')
    new_users = xml_serializer.deserialize(xml_serialized)

    json_serializer = UsersSerializer('json')
    assert new_users == json_serializer.deserialize(json_serializer.serialize(new_users))


if __name__ == "__main__":
    test_json_serialize()
    test_json_deserialize()
    test_xml_serialize()
    test_xml_deserialize()
    test_lxml_serialize()
    test_lxml_deserialize()
    test_save_xml()
    test_save_lxml()
    test_save_json()
    test_load_xml()
    test_load_lxml()
    test_load_json()
    test_invalid_file_path()
    test_invalid_converter()
    test_valid_converters()
    test_cross_format_conversion()
