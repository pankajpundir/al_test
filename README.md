# README #

## Problem Statement ##
In Python write a command line tool which shows how you would take some sets of personal data  (name, address, phone number) and serialise them/deserialise them in at least 2 formats (one should be XML), and display it in at least 2 different ways (no need to use a GUI Framework, command line is fine).  There is no need to support manual data entry - you could manually write a file in one of your chosen formats to give you your test data.
 
Write it in such a way that it would be easy for a developer:

1. to add support for additional storage formats
2. to query a list of currently supported formats
3. to supply an alternative XML reader/writer
 
This should ideally show Object-Oriented Design and Design Patterns Knowledge, we’re not looking for use of advanced Python constructs. Provide reasonable Unit Test coverage.

## Solution ##

### App Sample Usage ###

* **Show help**

reformat_users_app.py -h

* **List available converters**

reformat_users_app.py -show_converters

* **Serialize current default data into default converter, xml**

reformat_users_app.py -serialize

* **Serialize default data into json**

reformat_users_app.py -serialize -converter json

* **Serialize and save current default data set to a custom file**

reformat_users_app.py -serialize -save <file path of file to save>

* **Load custom input file for serializing data**

reformat_users_app.py -serialize -file <path to load data from custom file>

* **Deserialize xml string (-ds)**

reformat_users_app.py -converter xml -ds "<users><user><data><key>status</key><value>online</value></data><data><key>name</key><value>Pankaj Singh</value></data></user></users>"

* **Deserialize an input xml file**

reformat_users_app.py -converter xml -df <path to existing xml file>

### Data Samples ###

Users data is being stored in a list of dictionaries. This schema was chosen to provide flexibility in defining different possible keys per user.

Sample XML is so chosen to provide flexibility of adding keys with spaces,
~~~~{.xml}
<users>
    <user>
        <data>
            <key>name</key>
            <value>Pankaj Singh</value>
        </data>
        <data>
            <key>phone number</key>
            <value>123 456 7890</value>
        </data>
    </user>
</users>
~~~~

Sample JSON is as follows,
~~~~{.json}
'[{"name": "Pankaj Singh", "status": "Online"}]'
~~~~

### Converters ###

Converter classes are inherited from BaseConverter and their types are defined by the **name** attribute.

Each converter implements 4 methods

* serialize

Serializes the input Users object.

* dump

Dumps the serialized object into a file.

* deserialize

Deserializes the input string into Users object

* load

Deserializes input file into Users object.

**Note on XML converter**

Code sample provides 2 types of xml converters. First that uses **xml.etree** module and the other uses **lxml.etree**.
We are using dependency injection to better execute code reusage on these classes.

### Serializer ###

~~~~{.python}
serializer = UsersSerializer('xml')
~~~~

UsersSerializer class is an abstract implementation of the converters and can be initialized with one of the below converters,

1. xml
2. lxml
3. json


Current XML or JSON file can be deserialized,
~~~~{.python}
xml_file_path = "in.xml" # Path to input xml file
xml_serializer = UsersSerializer('xml')
users = xml_serializer.deserialize(xml_file_path)
~~~~

and the deserialized object can be serialized like so,
~~~~{.python}
xml_serializer = UsersSerializer('xml')
print(xml_serializer.deserialize(users)) # Where users is a deserialized object
~~~~

The same deserialized object can be serialized into a different format like so,
~~~~{.python}
xml_serializer = UsersSerializer('json')
print(xml_serializer.deserialize(users)) # Where users is a deserialized object
~~~~


My dummy data available in sample_data/input_data.txt, is a custom format.
~~~~
name
Pankaj Singh
status
online

name
Foo Bar
gender
unknown
~~~~

If users data is already available in one of the xml or json format (in the above spec) then Serializer can be used to convert into the other format like below,
~~~~{.python}
xml_file_path = "in.xml" # Path to input xml file
xml_serializer = UsersSerializer('xml')
users = xml_serializer.deserialize(xml_file_path)

json_serializer = UsersSerializer('json')
json_file_path = "out.json" # path of output json file
json_serializer.serialize(users)
~~~~

## Possible extensions ##
* We could auto detect the converted through input file extension. A converter variable could then be used to use a different flavor of same format.
* Application could be compiled into an executable to protect code integrity.

## Who do I talk to? ##

* Pankaj Singh <pankajspundir@gmail.com>